# Reinforcement Learning Forecast

This project carries all available data, code and results for the paper "An Artificial Intelligence Approach to Forecasting when there are Structural Breaks: a Reinforcement Learning based Framework for Fast Switching".
This paper was published in the academic journal [Empirical Economics](https://www.springer.com/journal/181).

## Getting started

This project consists in two parts:

- [ ] Simulation Experiment
- [ ] Real Data analysis

This project contains all the necessary folders to generate all the forecasts and information needed for this paper.
Many folders are empty, but they are present in this project because it allows generating all the results automatically.

We use Oxmetrics to generate forecasts from Autometrics, AR1 and Hendry's robust model. All other methods were done with Python.

## 1 - Simulation Experiment

This part refers to the "Simulation Experiment" section of the paper.

### Folders
We have three sets of folders that will hold the results of the experiment:

1. Agent Data - excel files of the Q-Table of the Agent.

2. Ox Files - Forecasting results from Oxmetrics to generate base forecasting models to be combined.
This folder also has the following files: 
- monte-carlo.ox: generate the simulated series with the break.
- PreparingDataForRL.ox - generate excel files with forecasting results for autometrics, robust model and AR1 for the simulated series.
- monte-carlo-estab.ox - generate the simulated series without break.

3. RL results - results of the Reinforcement Learning algorithm (has subfolders with Q-tables, results and result analysis).

### Files outside folders
This subsection also has the following files:
- "Monte Carlo".py - script to get all oxmetrics results and import in Python.
- q-table.xlsx - clean Q-Table that will be populated when the algorithm runs.
- "Rl Agent".py - algorithm to generate all the forecasts, besides the Oxmetrics ones, including the RL algorithm.
- "Result Analysis".py - script to analyse the data and generate results from data.

## 2 - Real Data Analysis

This part refers to the "Real Data Analysis" section of the paper.

### Folders

1 - Ox Results - empty folder to receive Oxmetrics results - forecasting models from OX.
2 - Python Results - empty folder to receive Python results - forecasting models from Python.

### Files outside folders

1 - FinalScript.py - Final script to generate all the python analysis, including the Reinforcement Learning algorithm.
2 - q-table.xlsx - clean Q-Table that will be populated when the algorithm runs.
3 - "Raw Data - 24_06_21".xlsx - consumer price index data from the Bank for International Settlements [BIS](https://www.bis.org/).

The data analysis was done during three different periods:

- those with data starting from January 1950 until May 2021;
- those with data starting from January 1950 until March 2021;
- those with data starting from January 1980 until March 2021.

Therefore we also have three types of files for those different periods:

1 - a FinalScript.py for one of those periods
2 - an "Ox Forecasts".ox for one of those periods
3 - a "Final Data".xlsx for one of those periods (this data was generated with its growth rate to avoid unit roots).

Each one of those can be run to generate each specific result.
