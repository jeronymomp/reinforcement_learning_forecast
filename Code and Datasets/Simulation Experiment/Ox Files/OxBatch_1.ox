#include <oxstd.oxh>
#import <packages/CATS/CATS>

run_1()
{
	//--- Ox code for CATS( 1)
	decl model = new CATS();

	model.Load("D:\\Dropbox\\FGV\\CEMAP\\pesquisas\\Jeronymo-ML\\previsoes.in7");
	model.Select("Y", {"Serie-1", 0, 0});
	model.Select("Y", {"Serie-2", 0, 0});
//	model.Select("U", {"Constant", 0, 0});

	model.Lags(2,2,2);
	model.I1Rank(2);
	model.Trend("DRIFT");
	model.SetSelSample(1902, 1, 1949, 1);
	model.SetMethod("RRR");
	model.Estimate();

	decl results=model.BootstrapRankTest();
				 println(results);
	delete model;
}

main()
{
	run_1();
}
