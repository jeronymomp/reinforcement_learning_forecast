# -*- coding: utf-8 -*-
"""
Created on Thu Oct  8 15:18:02 2020

@author: Jeronymo marcondes - CEMAP-FGV
"""

# =============================================================================
# Documentation
# File to extract results and apply DB test.
# =============================================================================


# =============================================================================
# 1 - Import Packages
# =============================================================================

import sys
import random
import pandas as pd
import numpy as np
import time
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from sklearn.model_selection import GridSearchCV
from sklearn import linear_model
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, LSTM
import numpy
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
import tensorflow as tf
from tensorflow import keras
import keras_tuner as kt
from keras_tuner import RandomSearch

# =============================================================================
# 2 - Extract files
# =============================================================================

countries = ['ARG','AUS','DNK','IRE','JPN','NZE','PER','AFR','AUT','BEL','CAN','SWI','CHL','GER','GBR','ISL','ITA','LUX','NOR','PRT','BRA','CYP','SPA','FIN','GRE','HKG','IDO','ISR','IND','KOR','MAL','MEX','MAL','NET','PHI','SIN','THA','TUR']


data = pd.read_csv(r'C:\Dados\GIT pessoal\reinforcement-learning-forecast\Release\3.0.0\Real Data\Price Index\Python Results\ForecastError\USA.csv',index_col=0 )

data.columns = ['USA_MaeAb','USA_Maesq']

for country in countries:
    
    new_data = pd.read_csv(r'C:\Dados\GIT pessoal\reinforcement-learning-forecast\Release\3.0.0\Real Data\Price Index\Python Results\ForecastError\{}.csv'.format(country),index_col=0 )

    new_data.columns = ['{}_MaeAb'.format(country),'{}_Maesq'.format(country)]

    data = data.merge(new_data,left_index=True, right_index=True)
    
#Prepare to export to latex
def truncate(n):
    return int(n * 1000) / 1000

#maesq
data_maesq = data.filter(regex='_Maesq')

col_countries = ['USA','ARG','AUS','DNK','IRE','JPN','NZE','PER','AFR','AUT','BEL','CAN','SWI','CHL','GER','GBR','ISL','ITA','LUX','NOR','PRT','BRA','CYP','SPA','FIN','GRE','HKG','IDO','ISR','IND','KOR','MAL','MEX','MAL','NET','PHI','SIN','THA','TUR']
data_maesq.columns = col_countries
data_maesq = data_maesq.transpose()

data_maesq.to_latex()

#mae
data_mae = data.filter(regex='_MaeAb')

col_countries = ['USA','ARG','AUS','DNK','IRE','JPN','NZE','PER','AFR','AUT','BEL','CAN','SWI','CHL','GER','GBR','ISL','ITA','LUX','NOR','PRT','BRA','CYP','SPA','FIN','GRE','HKG','IDO','ISR','IND','KOR','MAL','MEX','MAL','NET','PHI','SIN','THA','TUR']
data_mae.columns = col_countries
data_mae = data_mae.transpose()

data_mae.to_latex()

# =============================================================================
# 3 - DataFinal adjusted
# =============================================================================

countries = ['USA','ARG','AUS','DNK','IRE','JPN','NZE','PER','AFR','AUT','BEL','CAN','SWI','CHL','GER','GBR','ISL','ITA','LUX','NOR','PRT','BRA','CYP','SPA','FIN','GRE','HKG','IDO','ISR','IND','KOR','MAL','MEX','MAL','NET','PHI','SIN','THA','TUR']

for country in countries:
    data = pd.read_csv(r'C:\Dados\GIT pessoal\reinforcement-learning-forecast\Release\3.0.0\Real Data\Price Index\Python Results\Datafinal\{}.csv'.format(country),index_col=0 )
    
    data_adj = data.iloc[:,np.r_[1,2,3,6,8,10,12,14]]
    
    for i in range(0,len(data_adj)):
        if(data_adj.loc[i,'result_rl']=='ann'):
            data_adj.loc[i,'rl_forecast']=data_adj.loc[i,'AnnForecast']
        elif(data_adj.loc[i,'result_rl']=='auto'):
            data_adj.loc[i,'rl_forecast']=data_adj.loc[i,'AutometricsForecast']
        elif(data_adj.loc[i,'result_rl']=='ridge'):
            data_adj.loc[i,'rl_forecast']=data_adj.loc[i,'RidgeForecast']
        elif(data_adj.loc[i,'result_rl']=='lasso'):
            data_adj.loc[i,'rl_forecast']=data_adj.loc[i,'LassoForecast']
        elif(data_adj.loc[i,'result_rl']=='robust'):
            data_adj.loc[i,'rl_forecast']=data_adj.loc[i,'RobustForecast']
        else:
            data_adj.loc[i,'rl_forecast']=data_adj.loc[i,'Ar1']
                
                
    data_adj.loc[:,'result_rl_error'] = data_adj.loc[:,'OriginalData'] - data_adj.loc[:,'rl_forecast']
    data_adj.loc[:,'AnnForecast_error'] = data_adj.loc[:,'OriginalData'] - data_adj.loc[:,'AnnForecast']
    data_adj.loc[:,'AutometricsForecast_error'] = data_adj.loc[:,'OriginalData'] - data_adj.loc[:,'AutometricsForecast']
    data_adj.loc[:,'RidgeForecast_error'] = data_adj.loc[:,'OriginalData'] - data_adj.loc[:,'RidgeForecast']
    data_adj.loc[:,'LassoForecast_error'] = data_adj.loc[:,'OriginalData'] - data_adj.loc[:,'LassoForecast']
    data_adj.loc[:,'RobustForecast_error'] = data_adj.loc[:,'OriginalData'] - data_adj.loc[:,'RobustForecast']
    data_adj.loc[:,'Ar1_error'] = data_adj.loc[:,'OriginalData'] - data_adj.loc[:,'Ar1']
    
    data_adj.to_csv(r'C:\Dados\GIT pessoal\reinforcement-learning-forecast\Release\3.0.0\Real Data\Price Index\Python Results\Datafinal\Point_Error_{}.csv'.format(country))

# =============================================================================
# 4 - Diebold Mariano
# =============================================================================


from dm_test import dm_test
country = 'AUS'
DataFinal = pd.read_csv(r'C:\Dados\GIT pessoal\reinforcement-learning-forecast\Release\3.0.0\Real Data\Price Index\Python Results\Datafinal\{}.csv'.format(country),index_col=0 )

msq = dm_test(DataFinal.loc[:,'OriginalData'].tolist(),
             DataFinal.loc[:,'result_rl_error'].tolist(),
             DataFinal.loc[:,'AutometricsForecast_Error'].tolist(),h = 1, crit="MSE")
print(msq)



mae = dm_test(DataFinal.loc[:,'OriginalData'],
             DataFinal.loc[:,'result_rl_error'],
             DataFinal.loc[:,'AutometricsForecast_Error'],h = 1, crit="MAD")
print(mae)

# =============================================================================















