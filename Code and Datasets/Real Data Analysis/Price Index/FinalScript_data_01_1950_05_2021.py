# -*- coding: utf-8 -*-
"""
Created on Thu Oct  8 15:18:02 2020

@author: Jeronymo marcondes - CEMAP-FGV
"""
#Documententation

#Dependencies:

#1-Hyperparameters

#2-Methods
    
    #2.a - Search Forecasting Method - lookup for a best choice in Q-Table. 
#If there isn't = Exploration. If guessed error is the minimun,
#generate reward 1 for this choice

    #2.b - Learning and updating Belmann Equation. Here we compare our 
#estimate with the actual result. Update the Q table afterwards.

    #2.c - Adjusting final q Table.
    
    
#Steps:
#1 - Generate more prediction models
#2 - Import data from Ox
#3 - Get acumulated error according to some rule - manual part
#4 - Apply Rl to get results.
#----------------------------------------------------------------------------#

#Part 1 - Packages

#----------------------------------------------------------------------------#
#Import Packages
import sys
import random
import pandas as pd
import numpy as np
import time
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from sklearn.model_selection import GridSearchCV
from sklearn import linear_model
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, LSTM
import numpy
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
import tensorflow as tf
from tensorflow import keras
import keras_tuner as kt
from keras_tuner import RandomSearch
#----------------------------------------------------------------------------#

#Part 2 - Functions

#----------------------------------------------------------------------------#
#A - Search Forecasting Method - using memory

def UsingMemory(Dataset,q_table,row,str_methods,nb_methods):
    result_list = []
    #Search inputs from new data
    var = Dataset.iloc[row,nb_methods+1]
    #Get results from Q Table
    q_table_df = q_table[q_table.loc[:,'Signals']==var]
    if q_table_df.empty != True:     
        q_table_value = pd.Series(q_table_df.iloc[(len(q_table_df)-1),:])
        max_value = q_table_value[1:(1+nb_methods)].astype('float64').idxmax()
        result_list.append(max_value)
    else:
    #select values at random if they are not in the q table
        result_list.append(random.choice(str_methods))
    return result_list

#B - Learning and updating Belmann Equation

#B1 - Temporal difference with memory
def ReinforcementLearn(Dataset,q_table,row,result_list): 
    result_actual = []
    q_table_max_index_value = q_table.index.max()
    #Look for what actually happened -actual rewards => can be modified
    result_actual.append(Dataset.iloc[(row),(nb_methods+2)])
        #State reward
    if result_actual[0]==result_list[0]:
        reward = 2
    else:
        reward = 1
    #Get the value to be updated
    var = Dataset.iloc[row,(nb_methods+1)]
    q_table_df = q_table[q_table.loc[:,'Signals']==var]
    if reward == 2:
        string = len(q_table) + 1
        new_row = pd.DataFrame(np.array([[var, 1, 1, 1, 1, 1, 1]]),columns=columns)
        new_row.loc[:,result_list[0]] = 2
        q_table = q_table.append(new_row,ignore_index=True) 
    else:
        #If the value was seen before
        if q_table_df.empty != True:
            q_table_max_value_idx = q_table_df.index.max()
                        
            #Bellman equation
            #Set rewards for different strategies
            reward_winner = 3
            reward_looser = 2
            
            #Set all values to looser value (we will update obnly the winner later)
            #Looser:
            q_value_1 = q_table.loc[q_table_max_value_idx,result_list[0]]
            q_value = int(q_value_1) + alpha*(reward_looser-int(q_value_1))
            last_result = q_table.iloc[q_table_max_value_idx,:][0]
            q_table.loc[(q_table_max_index_value+1),:] = [last_result,q_value,q_value,q_value,q_value,q_value,q_value]
            
            #Winner
            winner_q_value_1 = q_table.loc[q_table_max_value_idx,result_actual[0]]
            winner_q_value = int(winner_q_value_1) + alpha*(reward_winner-int(winner_q_value_1))
            q_table.loc[(q_table_max_index_value+1),result_actual[0]] = winner_q_value
                 
        #Else we have to append the new result
        else:
            string = len(q_table) + 1
            new_row = pd.DataFrame(np.array([[var, 1, 1, 1, 1, 1, 1]]),columns=columns)
            new_row.loc[:,result_actual[0]] = 2
            q_table = q_table.append(new_row,ignore_index=True)           
    return q_table

#B2 - Temporal difference without memory
def TempDiff(Dataset,q_table,row,result_list): 
    result_actual = []
    q_table_max_index_value = q_table.index.max()
    #Look for what actually happened -actual rewards => can be modified
    result_actual.append(Dataset.iloc[(row),(nb_methods+2)])
        #State reward
    if result_actual[0]==result_list[0]:
        reward = 2
    else:
        reward = 1
    #Get the value to be updated
    var = Dataset.iloc[row,nb_methods+1]
    q_table_df = q_table[q_table.loc[:,'Signals']==var]
    if reward == 2:
        pass
    else:        
        reward_winner = 3
        reward_looser = 2    
        winning_method = Dataset.iloc[(row),(nb_methods+2)]
        looser_method = result_list[0]
        
        #If the value was seen before
        if q_table_df.empty != True:
            q_value_1 = float(q_table_df.iloc[0,1]) + alpha*(Dataset.iloc[row,(2)]- float(q_table_df.iloc[0,1]))
            lost_q_value = {'Signals':var,'auto':2,'robust':2,'ar1':2,'lasso':2,'ridge':2,'ann':2}
            new_row = pd.DataFrame(lost_q_value,index=[0])
            new_row.loc[:,winning_method] = float(new_row.loc[:,winning_method]) + alpha*(reward_winner-float(new_row.loc[:,winning_method]))
            new_row.loc[:,looser_method] = float(new_row.loc[:,looser_method]) + alpha*(reward_looser-float(new_row.loc[:,looser_method]))
            
            q_table.loc[q_table['Signals']==var] =  new_row   


        #Else we have to append the new result
        else:
            string = len(q_table) + 1
            #order: ar1, dd , ic and auto
            new_row = pd.DataFrame(np.array([[var, 2, 2, 2, 2, 2, 2]]),columns=columns)
            q_table = q_table.append(new_row,ignore_index=True)
            q_table.loc[q_table.loc[q_table['Signals']==var].index[0],winning_method] = 3
    return q_table


#C - Get intertemporal voting for best action

#Obs. It's not incorporated the intertemporal change.
#For now, the most common action will be taken.
def FinalQtable(q_table):
    #Transform into int and get the quantities of occurrences  
    q_table_int = q_table.iloc[:,1:len(q_table.columns)].astype(float) 
    q_table.iloc[:,1:len(q_table.columns)] = q_table_int
    intertemp_q_value = q_table.groupby(columns).size().reset_index(name='Count')
    intertemp_q_value = intertemp_q_value.sort_values(by=['Signals','Count'],ascending=False)
    intertemp_q_value = intertemp_q_value.drop_duplicates('Signals')
    q_table = intertemp_q_value
    return q_table

#D - Mean Absolute Percentage Error
def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean((2*np.abs(y_true - y_pred)) / np.abs(y_true + y_pred))

#E - ANN model - Dense
def ann_builder(hp):
  model = keras.Sequential()
  hp_units1 = hp.Int('units1', min_value=12, max_value=50, step=32)
  hp_units2 = hp.Int('units2', min_value=8, max_value=50, step=32)
  act_func = hp.Choice('dense_activation', values=['relu', 'tanh', 'sigmoid'], default='relu')
  hp_float = hp.Float('dropout',min_value=0.0,max_value=0.5)
  
  model.add(keras.layers.Dense(units=hp_units1, activation=act_func))
  model.add(keras.layers.Dropout(hp_float))
  model.add(keras.layers.Dense(units=hp_units2, activation=act_func))
  model.add(keras.layers.Dropout(hp_float))
  model.add(keras.layers.Dense(1))

  # Tune the learning rate for the optimizer
  # Choose an optimal value from 0.01, 0.001, or 0.0001
  hp_learning_rate = hp.Choice('learning_rate', values=[1e-7, 1e-8, 1e-9])

  model.compile(optimizer=keras.optimizers.Adam(learning_rate=hp_learning_rate),
               loss=keras.losses.BinaryCrossentropy(from_logits=True),
               metrics=['accuracy'])

  return model

#F- ANN model - LSTM
def lstm_builder(hp):
  model = keras.Sequential()
  hp_units1 = hp.Int('units1', min_value=12, max_value=50, step=32)
  hp_units2 = hp.Int('units2', min_value=8, max_value=50, step=32)
  act_func = hp.Choice('dense_activation', values=['relu', 'tanh', 'sigmoid'], default='relu')
  hp_float = hp.Float('dropout',min_value=0.0,max_value=0.5)
  
  model.add(keras.layers.LSTM(units=hp_units1, activation=act_func, input_shape = (X_train.shape[1], 3), return_sequences=True))
  model.add(keras.layers.Dropout(hp_float))
  model.add(keras.layers.LSTM(units=hp_units2, activation=act_func))
  model.add(keras.layers.Dropout(hp_float))
  model.add(keras.layers.Dense(1))

  # Tune the learning rate for the optimizer
  # Choose an optimal value from 0.01, 0.001, or 0.0001
  hp_learning_rate = hp.Choice('learning_rate', values=[1e-7, 1e-8, 1e-9])

  model.compile(optimizer=keras.optimizers.Adam(learning_rate=hp_learning_rate),
               loss=keras.losses.BinaryCrossentropy(from_logits=True),
               metrics=['accuracy'])
  return model
#----------------------------------------------------------------------------#

#Part 3 - Parameters

#----------------------------------------------------------------------------#

#Importing Paramaeters - data for initial aalysis discarding miss from OX
initial_data = 40
final_data = 855

#Training Parameters - used on Lasso, Ridge and Ann
train_set = 200
test_set = final_data - train_set
dependent_variable = ['OriginalData']
independent_variable = ['AutometricsForecast','RobustForecast','Ar1']

#Parameters for cumulated error calculation
cumulate_error = 4

# Hyperparameters for Rl
alpha = 0.9
gamma = 0.6
epsilon = 0.1

#Parameters for Rl
str_methods = ['auto','robust','ar1','lasso','ridge','ann']
nb_methods = 6 #number of forecasting methods
total_data = 855
rows = total_data - cumulate_error - 4  #total data - cumulated error - 
#4 steps to calculate de state (model, model, model)

#Format of rows to append in the q table
columns=['Signals','auto','robust','ar1','lasso','ridge','ann']

#Fields to be changed in the code  -- lost usage
#value_array 
#result_array
#[var, 1, 1, 1, 1] #look for those arrays on the second function

#Countries for looping algorithm
countries = ['AUT','BEL','CAN','SWI','CHL','GER','GBR','ISL','ITA','LUX','NOR','PRT','USA']

#----------------------------------------------------------------------------#

#Part 4 - For loop through results

#----------------------------------------------------------------------------#

for country in countries:
    
    ox_result = pd.read_csv(r'C:\Dados\GIT pessoal\reinforcement-learning-forecast\Release\3.0.0\Real Data\Price Index\Ox Results\1 Step\DataForecast_{}.csv'.format(country),sep=',')   
    ox_result = ox_result.iloc[initial_data:final_data,:]
    
    numpy.random.seed(0)
    
    #train and test data
    train_data = ox_result.iloc[0:train_set,:]
    test_data = ox_result.iloc[(train_set):,:]
    
    #train test split
    train_y = train_data.loc[:,dependent_variable]
    train_x = train_data.loc[:,independent_variable]
    test_y = test_data.loc[:,dependent_variable]
    test_x = test_data.loc[:,independent_variable]
    
    #1 - lasso regression
    lasso = linear_model.Lasso(alpha=0.5)
    lasso.fit(train_x, train_y)
    LassoForecast = lasso.predict(test_x)
    
    test_data.loc[:,'LassoForecast'] = LassoForecast
    test_data.loc[:,'LassoForecastError'] = test_data.loc[:,'OriginalData'] - test_data.loc[:,'LassoForecast'] 
    
    #2 - ridge regression
    ridge = linear_model.Ridge(alpha=0.5)
    ridge.fit(train_x, train_y)
    RidgeForecast = ridge.predict(test_x)
    
    test_data.loc[:,'RidgeForecast'] = RidgeForecast
    test_data.loc[:,'RidgeForecastError'] = test_data.loc[:,'OriginalData'] - test_data.loc[:,'RidgeForecast'] 
    
    #3 - Keras Ann (Dense or LSTM)
    
    #   Original model:
    #   model = tf.keras.models.Sequential([
    #   tf.keras.layers.Dense(12, activation='relu'),
    #   tf.keras.layers.Dense(8, activation='relu'),
    #   tf.keras.layers.Dense(1, activation='linear')
    # ])
    
    #   model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    
    #   model.fit(train_x, train_y, epochs=150, batch_size=10)
    
    #   AnnForecast = model.predict(test_x)
    
    
    #We changed for tunnable models with Keras:
        
    #3.1 - Tunnable Keras
    
    #3.1.1 - Dense
    tuner = kt.BayesianOptimization(ann_builder,
                objective='accuracy',
                max_trials=10,
                seed=42,
                executions_per_trial=2,
                overwrite=True)
    
    tuner.search(train_x, train_y, epochs=150)
    best_hps=tuner.get_best_hyperparameters(num_trials=1)[0]
    tunn_ann = tuner.hypermodel.build(best_hps)
    tunn_ann.fit(train_x, train_y, epochs=150)
    AnnForecast = tunn_ann.predict(test_x)
    
    test_data.loc[:,'AnnForecast'] = AnnForecast
    test_data.loc[:,'AnnForecastError'] = test_data.loc[:,'OriginalData'] - test_data.loc[:,'AnnForecast'] 
    
    
    #3.1.2 - LSTM
    X_train = []
    y_train = []
    for i in range(12, 200):
       X_train.append(train_x.iloc[i-12:i,0:3])
       y_train.append(train_y.iloc[i,0])
    X_train, y_train = np.array(X_train), np.array(y_train)
    X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 3))
    
    tuner = kt.BayesianOptimization(lstm_builder,
                objective='accuracy',
                max_trials=10,
                seed=42,
                executions_per_trial=2,
                overwrite=True)
    
    tuner.search(X_train, y_train, epochs=150)
    best_hps=tuner.get_best_hyperparameters(num_trials=1)[0]
    tunn_ann = tuner.hypermodel.build(best_hps)
    tunn_ann.fit(X_train, y_train, epochs=150)
    X_test = []
    for i in range(12, 615):
       X_test.append(test_x.iloc[i-12:i,0:3])
    X_test = np.array(X_test)
    X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 3))
    LstmForecast = tunn_ann.predict(X_test)
    
    #DataFinal
    DataFinal = test_data
    
    test_data.loc[:,'AutometricsForecast_Error'] = test_data.loc[:,'AutometricsForecast_Error']*test_data.loc[:,'AutometricsForecast_Error']
    test_data.loc[:,'RobustForecast_Error'] = test_data.loc[:,'RobustForecast_Error']*test_data.loc[:,'RobustForecast_Error']
    test_data.loc[:,'Ar1_Error'] = test_data.loc[:,'Ar1_Error']*test_data.loc[:,'Ar1_Error']
    test_data.loc[:,'AnnForecastError'] = test_data.loc[:,'AnnForecastError']*test_data.loc[:,'AnnForecastError']
    test_data.loc[:,'RidgeForecastError'] = test_data.loc[:,'RidgeForecastError']*test_data.loc[:,'RidgeForecastError']
    test_data.loc[:,'LassoForecastError'] = test_data.loc[:,'LassoForecastError']*test_data.loc[:,'LassoForecastError']
    test_data = test_data.iloc[:,np.r_[1,4,5,7,9,11,13]]
    
    test_data = test_data.reset_index(drop=True)
    #autometrics
    for j in range(len(test_data)-(cumulate_error)):
         Serie = test_data.iloc[(0+j):(cumulate_error+j),1].cumsum()
         Serie = Serie.reset_index(drop=True)
         Obs = Serie[len(Serie)-1]
         test_data.loc[((cumulate_error)+j),'auto']  = Obs     
    #Double Difference
    for j in range(len(test_data)-(cumulate_error)):
         Serie = test_data.iloc[(0+j):(cumulate_error+j),2].cumsum()
         Serie = Serie.reset_index(drop=True)
         Obs = Serie[len(Serie)-1]
         test_data.loc[((cumulate_error)+j),'robust']  = Obs     
    #ar1
    for j in range(len(test_data)-(cumulate_error)):
         Serie = test_data.iloc[(0+j):(cumulate_error+j),3].cumsum()
         Serie = Serie.reset_index(drop=True)
         Obs = Serie[len(Serie)-1]
         test_data.loc[((cumulate_error)+j),'ar1']  = Obs    
    #Lasso
    for j in range(len(test_data)-(cumulate_error)):
         Serie = test_data.iloc[(0+j):(cumulate_error+j),4].cumsum()
         Serie = Serie.reset_index(drop=True)
         Obs = Serie[len(Serie)-1]
         test_data.loc[((cumulate_error)+j),'lasso']  = Obs     
    #Ridge
    for j in range(len(test_data)-(cumulate_error)):
         Serie = test_data.iloc[(0+j):(cumulate_error+j),5].cumsum()
         Serie = Serie.reset_index(drop=True)
         Obs = Serie[len(Serie)-1]
         test_data.loc[((cumulate_error)+j),'ridge']  = Obs     
    #Ann
    for j in range(len(test_data)-(cumulate_error)):
         Serie = test_data.iloc[(0+j):(cumulate_error+j),6].cumsum()
         Serie = Serie.reset_index(drop=True)
         Obs = Serie[len(Serie)-1]
         test_data.loc[((cumulate_error)+j),'ann']  = Obs
         
    test_data = test_data.iloc[cumulate_error:,np.r_[0,7:13]]
    #Get the mean
    test_data.loc[:,'auto'] = test_data.loc[:,'auto'] / 5
    test_data.loc[:,'ar1'] = test_data.loc[:,'ar1'] / 5
    test_data.loc[:,'robust'] = test_data.loc[:,'robust'] / 5
    test_data.loc[:,'lasso'] = test_data.loc[:,'lasso'] / 5
    test_data.loc[:,'ridge'] = test_data.loc[:,'ridge'] / 5
    test_data.loc[:,'ann'] = test_data.loc[:,'ann'] / 5
    del Obs
    del Serie
    del j
    #Get minimun value column
    test_data = test_data.reset_index(drop=True)
    for h in range(0,(len(test_data))):
        test_data.loc[h,"result"] = test_data.iloc[h,1:7].astype('float64').idxmin()
    #Get Signal value column
    for h in range((cumulate_error-4),len(test_data)):
        test_data.loc[(4+h),"Signal"] = str(test_data.iloc[(1+h),7]) + ',' + str(test_data.iloc[(2+h),7]) + ',' + str(test_data.iloc[(3+h),7])
    test_data = test_data.iloc[cumulate_error:(len(test_data)-4),np.r_[0:7,8,7]]    
    
    q_table = pd.read_csv(r'C:\Dados\GIT pessoal\reinforcement-learning-forecast\Solution Testing\Real Data\q_table.csv',sep = ';')
    
    for row in range(0,len(test_data)):
        result_list = UsingMemory(test_data,q_table,row,str_methods,nb_methods)
        #q_table = TempDiff(test_data,q_table,row,result_list)   
        q_table = ReinforcementLearn(test_data,q_table,row,result_list)   
    
    q_table = FinalQtable(q_table) 
    
    result = []
    
    for row in range(0,len(test_data)):
        result.append(UsingMemory(test_data,q_table,row,str_methods,nb_methods)) 
    result_df = pd.DataFrame(result)
    result_df = result_df.rename(columns={0: "Forecast"})
    
    #test_data.loc[:,'result_rl'] = result_df.loc[:,'Forecast']
    
    DataFinal = DataFinal.iloc[6:,:]
    DataFinal = DataFinal.reset_index(drop=True)
    DataFinal.loc[:,'result_rl'] = result_df.loc[:,'Forecast']
    
    for i in range(0,len(DataFinal)):
        if(DataFinal.loc[i,'result_rl']=='ann'):
            DataFinal.loc[i,'result_rl_error']=DataFinal.loc[i,'AnnForecast']
        elif(DataFinal.loc[i,'result_rl']=='auto'):
            DataFinal.loc[i,'result_rl_error']=DataFinal.loc[i,'AutometricsForecast']
        elif(DataFinal.loc[i,'result_rl']=='ridge'):
            DataFinal.loc[i,'result_rl_error']=DataFinal.loc[i,'RidgeForecast']
        elif(DataFinal.loc[i,'result_rl']=='lasso'):
            DataFinal.loc[i,'result_rl_error']=DataFinal.loc[i,'LassoForecast']
        elif(DataFinal.loc[i,'result_rl']=='robust'):
            DataFinal.loc[i,'result_rl_error']=DataFinal.loc[i,'RobustForecast']
        else:
            DataFinal.loc[i,'result_rl_error']=DataFinal.loc[i,'Ar1']
            
    #Msqe
    AnnMsqe = mean_squared_error(DataFinal.loc[:,'OriginalData'], DataFinal.loc[:,'AnnForecast'])
    LassoMsqe = mean_squared_error(DataFinal.loc[:,'OriginalData'], DataFinal.loc[:,'LassoForecast'])
    RidgeMsqe = mean_squared_error(DataFinal.loc[:,'OriginalData'], DataFinal.loc[:,'RidgeForecast'])
    AutoMsqe = mean_squared_error(DataFinal.loc[:,'OriginalData'], DataFinal.loc[:,'AutometricsForecast'])
    RobustMsqe = mean_squared_error(DataFinal.loc[:,'OriginalData'], DataFinal.loc[:,'RobustForecast'])
    Ar1Msqe = mean_squared_error(DataFinal.loc[:,'OriginalData'], DataFinal.loc[:,'Ar1'])
    RlMsqe = mean_squared_error(DataFinal.loc[:,'OriginalData'], DataFinal.loc[:,'result_rl_error'])
    LstmMsqe = mean_squared_error(DataFinal.iloc[6:,1], LstmForecast)
    
    ErrorFrame = {'Msqe':[AnnMsqe,LassoMsqe,RidgeMsqe,AutoMsqe,RobustMsqe,Ar1Msqe,RlMsqe,LstmMsqe]}
    
    msqe_df = pd.DataFrame(ErrorFrame,index =['Ann', 'Lasso', 'Ridge', 'Auto', 'Robust','Ar1','Rl','Lstm'])
    
    #Mae
    Annmae = mean_absolute_error(DataFinal.loc[:,'OriginalData'], DataFinal.loc[:,'AnnForecast'])
    Lassomae = mean_absolute_error(DataFinal.loc[:,'OriginalData'], DataFinal.loc[:,'LassoForecast'])
    Ridgemae = mean_absolute_error(DataFinal.loc[:,'OriginalData'], DataFinal.loc[:,'RidgeForecast'])
    Automae = mean_absolute_error(DataFinal.loc[:,'OriginalData'], DataFinal.loc[:,'AutometricsForecast'])
    Robustmae = mean_absolute_error(DataFinal.loc[:,'OriginalData'], DataFinal.loc[:,'RobustForecast'])
    Ar1mae = mean_absolute_error(DataFinal.loc[:,'OriginalData'], DataFinal.loc[:,'Ar1'])
    Rlmae = mean_absolute_error(DataFinal.loc[:,'OriginalData'], DataFinal.loc[:,'result_rl_error'])
    LstmMsqe = mean_absolute_error(DataFinal.iloc[6:,1], LstmForecast)
    
    ErrorFrame = {'Mae':[Annmae,Lassomae,Ridgemae,Automae,Robustmae,Ar1mae,Rlmae,LstmMsqe]}
    
    mae_df = pd.DataFrame(ErrorFrame,index =['Ann', 'Lasso', 'Ridge', 'Auto', 'Robust','Ar1','Rl','Lstm'])
          
    error_frame = pd.merge(mae_df,msqe_df,left_index=True, right_index=True)
    
    #Save 
    DataFinal.to_csv(r'C:\Dados\GIT pessoal\reinforcement-learning-forecast\Release\3.0.0\Real Data\Price Index\Python Results\Datafinal\{}.csv'.format(country))
    
    error_frame.to_csv(r'C:\Dados\GIT pessoal\reinforcement-learning-forecast\Release\3.0.0\Real Data\Price Index\Python Results\ForecastError\{}.csv'.format(country))

#----------------------------------------------------------------------------#

#Part 5 - Diebold Mariano Test

#----------------------------------------------------------------------------#
from dm_test import dm_test

msq = dm_test(DataFinal.loc[:,'OriginalData'].tolist(),
             DataFinal.loc[:,'result_rl_error'].tolist(),
             DataFinal.loc[:,'AutometricsForecast'].tolist(),h = 1, crit="MSE")
print(msq)


mae = dm_test(DataFinal.loc[:,'OriginalData'],
             DataFinal.loc[:,'result_rl_error'],
             DataFinal.loc[:,'AutometricsForecast'],h = 1, crit="MAD")
print(mae)

q_table_analysis = q_table

q_table_analysis[q_table_analysis[:] == 2].count()
