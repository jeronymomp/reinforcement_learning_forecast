#include <oxstd.oxh>
#include <oxstd.oxh>
#include <oxdraw.oxh>
#include <oxprob.oxh>
#import <modelbase>
#import <simulator>
#import <packages/PcGive/pcgive>

GetData(serie_number)
{


//Get Forecast File
	decl AutoModel = new PcGive();
	AutoModel.Load("C:\\Dados\\GIT pessoal\\reinforcement-learning-forecast\\Release\\2.0.0\\Monte Carlo\\Ox Files\\previsoes-estab.in7");

//Get Error File
	decl ErrorModel = new PcGive();
	ErrorModel.Load("C:\\Dados\\GIT pessoal\\reinforcement-learning-forecast\\Release\\2.0.0\\Monte Carlo\\Ox Files\\previsoes-erros-estab.in7");

//Get Variables of the file
	decl Serie = AutoModel.GetVar(sprint("Serie-",serie_number));
	decl D_Serie = AutoModel.GetVar(sprint("DSerie-",serie_number));
	decl AR1 = AutoModel.GetVar(sprint("F-AR1-DSerie-",serie_number));
	decl DD = AutoModel.GetVar(sprint("F-DD-DSerie-",serie_number));
	decl IC = AutoModel.GetVar(sprint("F-IC-DSerie-",serie_number));
	decl AUTO = AutoModel.GetVar(sprint("F-Auto-DSerie-",serie_number));
	decl AR1_error = ErrorModel.GetVar(sprint("FE-AR1-DSerie-",serie_number));
	decl DD_error = ErrorModel.GetVar(sprint("FE-DD-DSerie-",serie_number));
	decl IC_error = ErrorModel.GetVar(sprint("FE-IC-DSerie-",serie_number));
	decl AUTO_error = ErrorModel.GetVar(sprint("FE-Auto-DSerie-",serie_number));

//Find average mean of forecasts
	decl AvgForec = (AR1 + DD + IC + AUTO) / 4;

//Find average forecast error
	decl AvgForec_error = D_Serie - AvgForec; 

//Set Result Matrix
	decl FinalMatrix = new matrix[rows(Serie)][6];

//Set Matrix Values													
	FinalMatrix[:][0] = Serie[:][0];
	FinalMatrix[:][1] = AR1_error[:][0];
	FinalMatrix[:][2] = DD_error[:][0];
	FinalMatrix[:][3] = IC_error[:][0];
	FinalMatrix[:][4] = AUTO_error[:][0];
	FinalMatrix[:][5] = AvgForec_error[:][0];

//Create object with results
	decl DataForecast = new Database();
	decl Names={"OriginalData","Ar1Error","DdError","IcError","AutoError","AvgForec_error"};
	DataForecast.Renew(FinalMatrix,Names);
	DataForecast.SaveCsv(sprint(sprint("C:\\Dados\\GIT pessoal\\reinforcement-learning-forecast\\Release\\2.0.0\\Monte Carlo\\Ox Files\\Results\\Serie_",serie_number,"_estab.csv")));

}
	

main()
{
			decl data;
			decl i;
			for(i=1;i<501;i++)
			{
				data = GetData(i);

			}

}
