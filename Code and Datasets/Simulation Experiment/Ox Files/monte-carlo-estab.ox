#include <oxstd.oxh>
#include <oxdraw.oxh>
#include <oxprob.oxh>
#import <modelbase>
#import <simulator>
#import <packages/PcGive/pcgive>



DGP(const mParametros, const iT, const iM, const iInicio, const mQuebra)
{
	decl i,j, mYP, iRegimes=rows(mQuebra), k, iReg, iCont=0 ;
    ranseed(-1);
    decl mErros = rann(iT+iInicio,iM);
	decl iR=columns(mParametros)-1;
	decl mYt=zeros(iT+iInicio,iM);
	println("teste",iR);
	   iCont=0;

	decl mRegimes=zeros(mQuebra[1],1);

//	println("OIIIIIIIIIIIIIIIIIIIIII", mQuebra);	
	
	
	if (iRegimes>1)
		{
			for	(k=1;k<iRegimes-1;k++)
	  		{ mRegimes=mRegimes|ones(mQuebra[k+1]-mQuebra[k],1)*k;
//			println("OIIIIIIIIIIIIIIIIIIIIII");	
		
			}
		}

	   
		for	(i=iR;i<iT+iInicio;i++)
		{
			iCont=mRegimes[i-1][0];
	//		println(sprint("Icont: ",iCont, " -> i",i));	
		

		
			mYP=zeros(iT+iInicio,1);
			 for(j=0;j<iR;j++)			// Defasagens
			 	{ 
			 	mYP=mYP+mParametros[iCont][j+1]*mYt[i-j-1][:];	  // Somando defasagens
			   	}
		 	mYt[i][:]=mParametros[iCont][0]+mYP+mErros[i][:];	// Somando constante
		}
	
		
  	decl model= new Modelbase();
	
		model.Create(1, 1900, 1,    1900+iT-1, 1);
			for	(i=0;i<iM;i++){
				model.Append(mYt[iInicio:iT+iInicio-1][i],sprint("Serie-",i+1));
				}
		model.SaveIn7("Dados-artificais-estab.in7");
	
	delete model;
}


modelo1(const iTempo, const iRep)
{

	decl k,j,dados,mFor=nans(iTempo,iRep+1),mRes,mFor1=nans(iTempo,iRep+1),mFor2=nans(iTempo,iRep+1),pvalor;
	decl prev;
	decl model = new PcGive();

	model.Load("C:\\Dados\\Git Pessoal\\reinforcement-learning-forecast\\Release\\2.0.0\\Monte Carlo\\Ox Files\\Dados-artificais-estab.in7");
	model.Deterministic(-1);

		for(j=1;j<iRep+1;j++)
		{
			dados=model.GetVar(sprint("Serie-",j));
			dados=diff(dados);
			model.Append(dados, sprint("DSerie-",j));
				for(k=20;k<iTempo-1;k++)
				{
				model.DeSelect();
				model.Select("Y", {sprint("DSerie-",j), 0, 0});
				model.Select("X", {"Constant", 0, 0});
				model.Select("Y", {sprint("DSerie-",j), 1, 1});
				model.SetSelSample(1900, 1, 1900+k, 1);
				model.SetMethod("OLS");
				model.Estimate();
				model.TestSummary();
				prev=model.Forecast(12);
				prev=prev[0]  ;
				mFor[k+1][j]=prev[0][0];  // Previsao do AR(1)
			//	model.PrintForecasts();

				mRes=model.GetResiduals();		// REsiduos
			//	println("Residuo: ",mRes);
				mFor1[k+1][j]=prev[0][0]+mRes[k-2][0];	   // Robusto

				println("Residuo Escolhido: ",mRes[k-2][0]);
				
				}

					   // Autometrics
			    for(k=20;k<iTempo-1;k++)
				{
				model.DeSelect();
				model.Select("Y", {sprint("DSerie-",j), 0, 1});
				model.Select("U", {"Constant", 0, 0});
//				model.Select("Y", {sprint("DSerie-",j), 1, 1});
				pvalor=10/(1*(3*k+1));	// E(retetion)=5
				model.Autometrics(pvalor, "IIS+SIS+DIIS", 0);
				model.AutometricsSet("print", 0); // 0 ou 1
				model.SetSelSample(1900, 1, 1900+k, 1);
				model.SetMethod("OLS");
				model.Estimate();


				prev=model.Forecast(12);
				prev=prev[0]  ;
				mFor2[k+1][j]=prev[0][0];
				}
			model.Append(mFor[:][j], sprint("F-AR1-DSerie-",j));   // Previsao do AR(1)
			model.Append(lag(dados,1), sprint("F-DD-DSerie-",j));  // Previsao do DD
			model.Append(mFor1[:][j], sprint("F-IC-DSerie-",j));	// Previsao do IC
			model.Append(mFor2[:][j], sprint("F-Auto-DSerie-",j));	 // Previso do Autometrics
			


			
		}

		model.SaveIn7("previsoes-estab.in7");
	delete model;


}

Erro(const iK){
	decl i,k,j;
	decl prev, prev1;

	decl sNome={"F-AR1-DSerie-","F-DD-DSerie-","F-IC-DSerie-","F-Auto-DSerie-"};

	decl model = new PcGive();

		model.Load("C:\\Dados\\Git Pessoal\\reinforcement-learning-forecast\\Release\\2.0.0\\Monte Carlo\\Ox Files\\previsoes-estab.in7");
		decl mD=model.GetVar("DSerie-1");
		decl iT=rows(mD);
		
		decl sExcluir=model.GetAllNames();
		
		
		for  (j=1;j<iK+1;j++)
		{
			sNome={sprint("DSerie-",j),sprint("F-AR1-DSerie-",j),sprint("F-DD-DSerie-",j),sprint("F-IC-DSerie-",j),sprint("F-Auto-DSerie-",j)};
			prev=model.GetVar(sNome);
			sNome={sprint("FE-AR1-DSerie-",j),sprint("FE-DD-DSerie-",j),sprint("FE-IC-DSerie-",j),sprint("FE-Auto-DSerie-",j)};
			prev1=zeros(iT,4);
				for  (i=0;i<4;i++)
					{	
						prev1[:][i]=prev[:][i+1]-prev[:][0]; 
				   }

				   println(prev1);
				   println(sNome);
	   		model.Append(prev1,sNome);	
				
		}

		model.Remove(sExcluir);
		model.SaveIn7("previsoes-erros-estab.in7");
	delete model;
}


modelos(const iTempo, const iRep)
{

	modelo1(iTempo, iRep);
	Erro(iRep);

			  
}

main()
{
	decl mParametros=<0, 1.5, -0.5;  // mu , rho=1, rho=2  - Delta Y = mu + ro*Delta yt-1 + et  ou Yt= u + ro1+yt-1+ro2*yt-2 + et
						0, 1.5, -0.5;
					  0, 1.5, -0.5;
					 0, 1.5, -0.5>;
	decl mQuebra=<0;120;220;320;420>;
 	decl iTempo=400, iDesc=20, iRep=3;		  // Tempo, N do MC e Descarte Inicial
	
	DGP(mParametros, iTempo, iRep, iDesc, mQuebra);	  // 	 DGP(mParametros, Tempo, Replicacoes, Descarte);
	modelos(iTempo, iRep);
}
