#include <oxstd.oxh>
#import <packages/PcGive/pcgive_ects>
#include <oxdraw.oxh>
#include <oxprob.oxh>
#import <modelbase>
#import <simulator>
#include <packages/x12ox/x12ox.oxh>
#import <packages/PcGive/pcgive_switching>

//This code implements the automatic part of Autometrics base model estimation as Robust Method Forecast

//This method implements all forecasting methods

ForecastFunction(const InitialSample, const FinalSample, const ForecastStep, const LagTestWindow, const ForecastHorizon, const Variable)

{

//Get Data to be forecast
	decl ForData = loadmat("C:\\Dados\\Git Pessoal\\reinforcement-learning-forecast\\Release\\3.0.0\\Real Data\\Price Index\\final_data.xlsx");

//Get principal variable to matrix
	decl Principal = new Database();
	Principal.Load("C:\\Dados\\Git Pessoal\\reinforcement-learning-forecast\\Release\\3.0.0\\Real Data\\Price Index\\final_data.xlsx");

	decl Var = Principal.GetVar(Variable);

//Global Variables
	decl h = ForecastHorizon;
	decl ForecMatrix = new matrix[rows(ForData)][7];

//Start for loop with forecast over rows
	for(h=0; h<ForecastHorizon; h++)
	{
//Start seed
				ranseed(0);

				//1 - AUTOMETRICS - ROBUST AND REGULAR VERSION	- outlier = "none"
				
//1.0 - Declare Autometrics - robust and regular models
				decl AutoModel = new PcGive();
							
//1.1 - Create Model with Autometrics
 				AutoModel.Load("C:\\Dados\\Git Pessoal\\reinforcement-learning-forecast\\Release\\3.0.0\\Real Data\\Price Index\\final_data.xlsx");
				AutoModel.Deterministic(-1);
				AutoModel.DeSelect();
				AutoModel.Select("Y", {Variable, 0, LagTestWindow});
				AutoModel.Select("X", {"Constant", 0, 0});
				AutoModel.Autometrics(0.01, "none", 1);

//1.2 - Model Settings and estimation
				AutoModel.SetSelSample(InitialSample, 1, FinalSample + h, 1);
				AutoModel.SetMethod("OLS");
				AutoModel.Estimate();

//1.3 - Model Forecast
				AutoModel.TestSummary();
				decl RobustForec = AutoModel.Forecast(ForecastStep, 0, 1, 1, 1);
				decl Forec = AutoModel.Forecast(ForecastStep);

//1.4 - Send results to matrix
				ForecMatrix[FinalSample+h][2] = RobustForec[0][1];
				ForecMatrix[FinalSample+h][1] = Forec[0];
				ForecMatrix[][0] = Var;

				//2 - AR1
			
//2.0 - Declare Autometrics - robust and regular models
				decl Ar1 = new PcGive();
							
//2.1 - Create Model with Autometrics
 				Ar1.Load("C:\\Dados\\Git Pessoal\\reinforcement-learning-forecast\\Release\\3.0.0\\Real Data\\Price Index\\final_data.xlsx");
				Ar1.Deterministic(-1);
				Ar1.Select("Y", {Variable, 0, 0});
				Ar1.Select("X", {"Constant", 0, 0});
				Ar1.Select("Y", {Variable, 1, 1});

//2.2 - Model Settings and estimation
				Ar1.SetSelSample(InitialSample, 1, FinalSample + h, 1);
				Ar1.SetMethod("OLS");
				Ar1.Estimate();

//2.3 - Model Forecast
				decl Ar1_Forec = Ar1.Forecast(ForecastStep);

//2.4 - Send results to matrix
				ForecMatrix[FinalSample+h][5] = Ar1_Forec[0][0];

	}

//Get forecast Error
ForecMatrix[][3] = ForecMatrix[][0] - ForecMatrix[][1];
ForecMatrix[][4] = ForecMatrix[][0] - ForecMatrix[][2];
ForecMatrix[][6] = ForecMatrix[][0] - ForecMatrix[][5];

				
//Create object with results
	decl DataForecast = new Database();
	decl Names={"OriginalData","AutometricsForecast","RobustForecast","AutometricsForecast_Error","RobustForecast_Error","Ar1","Ar1_Error"};
	DataForecast.Renew(ForecMatrix,Names);
	DataForecast.SaveIn7(sprint("C:\\Dados\\Git Pessoal\\reinforcement-learning-forecast\\Release\\3.0.0\\Real Data\\Price Index\\Ox Results\\1 Step\\DataForecast_",Variable,".in7"));
	DataForecast.SaveCsv(sprint("C:\\Dados\\Git Pessoal\\reinforcement-learning-forecast\\Release\\3.0.0\\Real Data\\Price Index\\Ox Results\\1 Step\\DataForecast_",Variable,".csv"));
	
}

main()
{

//List of coutries to iterate over
	        decl VariableList = {"AUT","BEL","CAN","SWI","CHL","GER","GBR","ISL","ITA","LUX","NOR","PRT","USA"};
			
//Function to apply via loop
			decl i;
			for(i=0;i<rows(VariableList);i++)
				{
					ForecastFunction(1,20,1,5,835,VariableList[i]);
				}

}
