# -*- coding: utf-8 -*-
"""
Created on Thu Aug  5 07:47:23 2021

@author: jeron
"""

import random
import pandas as pd
import numpy as np
import time
import matplotlib.pyplot as plt
import matplotlib.animation as animation


#Monte Carlo

#Result competing forecast
data = pd.read_csv('C:/Dados/Git Pessoal/reinforcement-learning-forecast/Release/3.0.0/Monte Carlo/RL results/Forecast Result/compet_result_500_estab.csv',index_col=0)
lista = []
for i in range(1,500):
    lista.append(data.iloc[:,i].idxmin())
    
#MSQE
data = pd.read_csv('C:/Dados/Git Pessoal/reinforcement-learning-forecast/Release/3.0.0/Monte Carlo/RL results/Variance and MSQE/msqe_499_estab.csv',index_col=0)
lista = []
for i in range(1,499):
    lista.append(data.iloc[:,i].idxmin())

lista.count('ar1')

#Variance mean
data = pd.read_csv('C:/Dados/Git Pessoal/reinforcement-learning-forecast/Release/3.0.0/Monte Carlo/RL results/Variance and MSQE/msqe_499_estab.csv',index_col=0)
data.mean(axis=1)

lista.count('rl')


#Plot
data = pd.read_csv('C:/Dados/Git Pessoal/reinforcement-learning-forecast/Release/3.0.0/Monte Carlo/Agent Data/agent_data_1_estab.csv',index_col=0)
plt.plot(data['OriginalData'])

lista.count('rl')

#Histogram
data = pd.read_csv('C:/Dados/Git Pessoal/reinforcement-learning-forecast/Release/3.0.0/Monte Carlo/RL results/Histograms/histogram_500_estab.csv',index_col=0)
data['actual_199'].value_counts()
data['result_199'].value_counts()

lista.count('rl')


#Real Data

