# -*- coding: utf-8 -*-
"""
Created on Tue Aug 11 17:17:45 2020

@author: jeronymo marcondes - CEMAP-FGV
"""
#----------------------------------------------------------------------------#
#Import Packages
import random
import pandas as pd
import numpy as np
import time
import matplotlib.pyplot as plt
import matplotlib.animation as animation
#----------------------------------------------------------------------------#
#Documententation

#Dependencies:

#1-Hyperparameters

#2-Methods
    
    #2.a - Search Forecasting Method - lookup for a best choice in Q-Table. 
#If there isn't = Exploration. If guessed error is the minimun,
#generate reward 1 for this choice

    #2.b - Learning and updating Belmann Equation. Here we compare our 
#estimate with the actual result. Update the Q table afterwards.


#----------------------------------------------------------------------------#
#Import Dataset
q_table = pd.read_csv('q_table.csv',sep = ';')
Dataset = pd.read_csv('agent_data_1.csv',sep=',')
#----------------------------------------------------------------------------#
# Hyperparameters
alpha = 0.9
gamma = 0.6
epsilon = 0.1

#Parameters
str_methods = ['ar1','dd','ic','auto','avg_forec']
nb_methods = 5 #number of forecasting methods
row = 15  #where we start our analysis

#Format of rows to append in the q table
columns=['Signals','ar1','dd','ic','auto','avg_forec']

#Fields to be changed in the code  -- lost usage
#look for those arrays on the second function:
[var, 1, 1, 1, 1] , 
[last_result,q_value,q_value,q_value,q_value,q_value] 


#----------------------------------------------------------------------------#
#Search Forecasting Method - using memory

def UsingMemory(Dataset,q_table,row,str_methods,nb_methods):
    result_list = []
    #Search inputs from new data
    var = Dataset.iloc[row,nb_methods+2]
    #Get results from Q Table
    q_table_df = q_table[q_table.loc[:,'Signals']==var]
    if q_table_df.empty != True:     
        q_table_value = pd.Series(q_table_df.iloc[(len(q_table_df)-1),:])
        max_value = q_table_value[1:(1+nb_methods)].astype('float64').idxmax()
        result_list.append(max_value)
    else:
    #select values at random if they are not in the q table
        result_list.append(random.choice(str_methods))
    return result_list

#----------------------------------------------------------------------------#
#Learning and updating Belmann Equation

def ReinforcementLearn(Dataset,q_table,row,result_list): 
    result_actual = []
    q_table_max_index_value = q_table.index.max()
    #Look for what actually happened -actual rewards => can be modified
    result_actual.append(Dataset.iloc[(row),(nb_methods+3)])
        #State reward
    if result_actual[0]==result_list[0]:
        reward = 2
    else:
        reward = 1
    #Get the value to be updated
    var = Dataset.iloc[row,nb_methods+2]
    q_table_df = q_table[q_table.loc[:,'Signals']==var]
    if reward == 2:
        string = len(q_table) + 1
        new_row = pd.DataFrame(np.array([[var, 1, 1, 1, 1, 1]]),columns=columns)
        new_row.loc[:,result_list[0]] = 2
        q_table = q_table.append(new_row,ignore_index=True) 
    else:
        #If the value was seen before
        if q_table_df.empty != True:
            q_table_max_value_idx = q_table_df.index.max()
                        
            #Bellman equation
            #Set rewards for different strategies
            reward_winner = 3
            reward_looser = 2
            
            #Set all values to looser value (we will update obnly the winner later)
            #Looser:
            q_value_1 = q_table.loc[q_table_max_value_idx,result_list[0]]
            q_value = int(q_value_1) + alpha*(reward_looser-int(q_value_1))
            last_result = q_table.iloc[q_table_max_value_idx,:][0]
            q_table.loc[(q_table_max_index_value+1),:] = [last_result,q_value,q_value,q_value,q_value,q_value]
            
            #Winner
            winner_q_value_1 = q_table.loc[q_table_max_value_idx,result_actual[0]]
            winner_q_value = int(winner_q_value_1) + alpha*(reward_winner-int(winner_q_value_1))
            q_table.loc[(q_table_max_index_value+1),result_actual[0]] = winner_q_value
                 
        #Else we have to append the new result
        else:
            string = len(q_table) + 1
            new_row = pd.DataFrame(np.array([[var, 1, 1, 1, 1, 1]]),columns=columns)
            new_row.loc[:,result_actual[0]] = 2
            q_table = q_table.append(new_row,ignore_index=True)           
    return q_table

#----------------------------------------------------------------------------#
#Get intertemporal voting for best action

#Obs. It's not incorporated the intertemporal change.
#For now, the most common action will be taken.
def FinalQtable(q_table):
    #Transform into int and get the quantities of occurrences  
    q_table_int = q_table.iloc[:,1:len(q_table.columns)].astype(float) 
    q_table.iloc[:,1:len(q_table.columns)] = q_table_int
    intertemp_q_value = q_table.groupby(columns).size().reset_index(name='Count')
    intertemp_q_value = intertemp_q_value.sort_values(by=['Signals','Count'],ascending=False)
    intertemp_q_value = intertemp_q_value.drop_duplicates('Signals')
    q_table = intertemp_q_value
    return q_table

#----------------------------------------------------------------------------#

#Generate Q Table
for row in range(1,350):
    result_list = UsingMemory(Dataset,q_table,row,str_methods,nb_methods)
    q_table = ReinforcementLearn(Dataset,q_table,row,result_list)
    
q_table = FinalQtable(q_table)
    
#Testing results
result = []

#Estimated
for row in range(1,350):
    result.append(UsingMemory(Dataset,q_table,row,str_methods,nb_methods))
    
result_df = pd.DataFrame(result)

#Actual Result
actual_result = Dataset.iloc[1:350,nb_methods+3]
actual_result_df = pd.DataFrame(actual_result)

#Merging Dataframes

#Set index
idx = []
for i in range(1,350):
    idx.append(i)
    
result_df = result_df.set_index([idx])
result_df = result_df.rename(columns={0: "Forecast"})

#Merging
final = actual_result_df.merge(result_df,left_index=True, right_index=True)

for row in range(1,349):

    print("Our estimate was model " + final.iloc[row,1] + " and the actual result was " + final.iloc[row,0])

test = final.iloc[:,0]==final.iloc[:,1]
test[test==True].count()

#----------------------------------------------------------------------------#
#Forecast competition

compet_models = Dataset.iloc[:,1:7]
compet_models = compet_models.iloc[1:350,:]
compet_models.loc[:,'binary_result'] = result

for i in range(1,350):
    if compet_models.loc[i,'binary_result']=='ar1':
        compet_models.loc[i,'rl_result'] = compet_models.loc[i,'ar1']
    elif compet_models.loc[i,'binary_result']=='auto':
        compet_models.loc[i,'rl_result'] = compet_models.loc[i,'auto']
    elif compet_models.loc[i,'binary_result']=='dd':
        compet_models.loc[i,'rl_result'] = compet_models.loc[i,'dd']
    elif compet_models.loc[i,'binary_result']=='avg_forec':
        compet_models.loc[i,'rl_result'] = compet_models.loc[i,'avg_forec']
    else:
        compet_models.loc[i,'rl_result'] = compet_models.loc[i,'ic']
        
compet_models.loc[:,'ar1_quad'] = compet_models.loc[:,'ar1']*compet_models.loc[:,'ar1']
compet_models.loc[:,'auto_quad'] = compet_models.loc[:,'auto']*compet_models.loc[:,'auto']
compet_models.loc[:,'dd_quad'] = compet_models.loc[:,'dd']*compet_models.loc[:,'dd']
compet_models.loc[:,'ic_quad'] = compet_models.loc[:,'ic']*compet_models.loc[:,'ic']
compet_models.loc[:,'rl_result_quad'] = compet_models.loc[:,'rl_result']*compet_models.loc[:,'rl_result']
compet_models.loc[:,'avg_forec_quad'] = compet_models.loc[:,'avg_forec']*compet_models.loc[:,'avg_forec']  
 
compet_models_result = compet_models.iloc[:,8:15].sum(axis = 0, skipna = True)   

compet_result = pd.DataFrame()
compet_result.loc[:,'3'] = compet_models_result

#----------------------------------------------------------------------------#
#Matrix to verify Variance
file = pd.read_csv(r'C:\Dados\GIT pessoal\reinforcement-learning-forecast\Release\2.0.0\Monte Carlo\Ox Files\Results\Serie_1_estab.csv')
File = file.iloc[21:400,:]
File = File.reset_index()
File = File.iloc[:,2:len(File.columns)]
#Adjusting with training data
File = File.iloc[8:357,:]
File = File.reset_index()
#Joining with binary result
result_rl = pd.read_csv(r'C:\Dados\GIT pessoal\reinforcement-learning-forecast\Release\3.0.0\Monte Carlo\RL results\Histograms\Histogram_500_estab.csv')
result_rl_serie = result_rl.loc[:,'result_1']
File.loc[:,'binary_result'] = result_rl_serie

#If else for finding variance
for i in range(1,349):
    if File.loc[i,'binary_result']=='ar1':
        File.loc[i,'rl_result'] = File.loc[i,'Ar1Error']
    elif File.loc[i,'binary_result']=='auto':
        File.loc[i,'rl_result'] = File.loc[i,'AutoError']
    elif File.loc[i,'binary_result']=='dd':
        File.loc[i,'rl_result'] = File.loc[i,'DdError']
    elif File.loc[i,'binary_result']=='avg_forec':
        File.loc[i,'rl_result'] = File.loc[i,'AvgForec_error']
    else:
        File.loc[i,'rl_result'] = File.loc[i,'IcError']
        
#Result
rl_result = File.loc[:,'rl_result']
ar1_result = File.loc[:,'Ar1Error']
auto_result = File.loc[:,'AutoError']
dd_result = File.loc[:,'DdError']
ic_result = File.loc[:,'IcError']
avg_result = File.loc[:,'AvgForec_error']


#Variances
ar1_var = np.var(ar1_result)
rl_var = np.var(rl_result)
auto_var = np.var(auto_result)
dd_var = np.var(dd_result)
ic_var = np.var(ic_result)
avg_var = np.var(avg_result)

#Order ar1, rl, auto, dd and ic
var_df = []
var_df.append(ar1_var)
var_df.append(rl_var)
var_df.append(auto_var)
var_df.append(dd_var)
var_df.append(ic_var)
var_df.append(avg_var)

#Set Dataframe
df_var = pd.DataFrame()
df_var['1'] = var_df

df_var.index = ['ar1','rl','auto','dd','ic','avg']

#----------------------------------------------------------------------------#
#For loop through all data

#1 - Generate Models
import random
import pandas as pd
import numpy as np
import time
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import os
#Get WD
#cwd = os.getcwd()
Histogram = pd.DataFrame()
compet_result = pd.DataFrame()
alpha = 0.9
gamma = 0.6
epsilon = 0.1
str_methods = ['ar1','dd','ic','auto','avg_forec']
nb_methods = 5 
row = 350
columns=['Signals','ar1','dd','ic','auto','avg_forec']
Data = range(1,501)

for d in Data:    
    q_table = pd.read_csv(r'C:\Dados\GIT pessoal\reinforcement-learning-forecast\Release\3.0.0\Monte Carlo\q_table.csv',sep = ';')
    Dataset = pd.read_csv(r'C:\Dados\GIT pessoal\reinforcement-learning-forecast\Release\3.0.0\Monte Carlo\Agent Data\agent_data_{}.csv'.format(d),sep=',')

    for row in range(1,row):
        result_list = UsingMemory(Dataset,q_table,row,str_methods,nb_methods)
        q_table = ReinforcementLearn(Dataset,q_table,row,result_list)   
    q_table = FinalQtable(q_table)
    result = []
    for row in range(1,350):
        result.append(UsingMemory(Dataset,q_table,row,str_methods,nb_methods))   
    result_df = pd.DataFrame(result)
    actual_result = Dataset.iloc[1:350,nb_methods+3]
    actual_result_df = pd.DataFrame(actual_result)
    idx = []
    for i in range(1,350):
        idx.append(i)   
    result_df = result_df.set_index([idx])
    result_df = result_df.rename(columns={0: "Forecast"})
    final = actual_result_df.merge(result_df,left_index=True, right_index=True)
    Histogram.loc[:,'actual_{}'.format(d)] = actual_result
    Histogram.loc[:,'result_{}'.format(d)] = result_df.values.tolist()
    Histogram.to_csv(r'C:\Dados\GIT pessoal\reinforcement-learning-forecast\Release\3.0.0\Monte Carlo\RL results\Histograms\histogram_{}.csv'.format(d))
    q_table.to_csv(r'C:\Dados\GIT pessoal\reinforcement-learning-forecast\Release\3.0.0\Monte Carlo\RL results\Q Tables\q_table_{}.csv'.format(d))
    compet_models = Dataset.iloc[:,1:nb_methods+2]
    compet_models = compet_models.iloc[1:350,:]
    compet_models.loc[:,'binary_result'] = result   
    for i in range(1,350):
        if compet_models.loc[i,'binary_result']=='ar1':
            compet_models.loc[i,'rl_result'] = compet_models.loc[i,'ar1']
        elif compet_models.loc[i,'binary_result']=='auto':
            compet_models.loc[i,'rl_result'] = compet_models.loc[i,'auto']
        elif compet_models.loc[i,'binary_result']=='dd':
            compet_models.loc[i,'rl_result'] = compet_models.loc[i,'dd']
        elif compet_models.loc[i,'binary_result']=='avg_forec':
            compet_models.loc[i,'rl_result'] = compet_models.loc[i,'avg_forec']
        else:
            compet_models.loc[i,'rl_result'] = compet_models.loc[i,'ic']         
    compet_models.loc[:,'ar1_quad'] = compet_models.loc[:,'ar1']*compet_models.loc[:,'ar1']
    compet_models.loc[:,'auto_quad'] = compet_models.loc[:,'auto']*compet_models.loc[:,'auto']
    compet_models.loc[:,'dd_quad'] = compet_models.loc[:,'dd']*compet_models.loc[:,'dd']
    compet_models.loc[:,'ic_quad'] = compet_models.loc[:,'ic']*compet_models.loc[:,'ic']
    compet_models.loc[:,'rl_result_quad'] = compet_models.loc[:,'rl_result']*compet_models.loc[:,'rl_result']
    compet_models.loc[:,'avg_forec_quad'] = compet_models.loc[:,'avg_forec']*compet_models.loc[:,'avg_forec']      
    compet_models_result = compet_models.iloc[:,(nb_methods+3):len(compet_models.columns)].sum(axis = 0, skipna = True)   
    compet_result.loc[:,'{}'.format(d)] = compet_models_result
    compet_result.to_csv(r'C:\Dados\GIT pessoal\reinforcement-learning-forecast\Release\3.0.0\Monte Carlo\RL results\Forecast Result\compet_result_{}.csv'.format(d))
 

#2 - Generate Matrix of variance
df_var = pd.DataFrame()
df_res = pd.DataFrame()
for j in range(1,500):
    #Matrix to verify Variance
    file = pd.read_csv(r'C:\Dados\GIT pessoal\reinforcement-learning-forecast\Release\3.0.0\Monte Carlo\Ox Files\Results\Serie_{}.csv'.format(j))
    File = file.iloc[21:400,:]
    File = File.reset_index()
    File = File.iloc[:,2:len(File.columns)]
    #Adjusting with training data
    File = File.iloc[8:357,:]
    File = File.reset_index()
    #Joining with binary result
    result_rl = pd.read_csv(r'C:\Dados\GIT pessoal\reinforcement-learning-forecast\Release\3.0.0\Monte Carlo\RL results\Histograms\Histogram_500.csv')
    y = j
    x = 'result_' + str(y) + ''
    result_rl_serie = result_rl.loc[:,x]
    File.loc[:,'binary_result'] = result_rl_serie
    
    for i in range(1,349):
        if File.loc[i,'binary_result']=='ar1':
            File.loc[i,'rl_result'] = File.loc[i,'Ar1Error']
        elif File.loc[i,'binary_result']=='auto':
            File.loc[i,'rl_result'] = File.loc[i,'AutoError']
        elif File.loc[i,'binary_result']=='dd':
            File.loc[i,'rl_result'] = File.loc[i,'DdError']
        elif File.loc[i,'binary_result']=='avg_forec':
            File.loc[i,'rl_result'] = File.loc[i,'AvgForec_error']
        else:
            File.loc[i,'rl_result'] = File.loc[i,'IcError']
            
    #Result
    rl_result = File.loc[:,'rl_result']
    ar1_result = File.loc[:,'Ar1Error']
    auto_result = File.loc[:,'AutoError']
    dd_result = File.loc[:,'DdError']
    ic_result = File.loc[:,'IcError']
    avg_result = File.loc[:,'AvgForec_error']
    
    #Variances
    ar1_var = np.var(ar1_result)
    rl_var = np.var(rl_result)
    auto_var = np.var(auto_result)
    dd_var = np.var(dd_result)
    ic_var = np.var(ic_result)
    avg_var = np.var(avg_result)
    
    #Order ar1, rl, auto, dd and ic
    var_df = []
    var_df.append(ar1_var)
    var_df.append(rl_var)
    var_df.append(auto_var)
    var_df.append(dd_var)
    var_df.append(ic_var)
    var_df.append(avg_var)
    
    #MSQE
    ar1_res = (ar1_result*ar1_result).sum() / len(ar1_result)
    rl_res = (rl_result*rl_result).sum() / len(rl_result)
    auto_res = (auto_result*auto_result).sum() / len(auto_result)
    dd_res = (dd_result*dd_result).sum() / len(dd_result)
    ic_res = (ic_result*ic_result).sum() / len(ic_result)
    avg_res = (avg_result*avg_result).sum() / len(avg_result)

    
    #Order ar1, rl, auto, dd and ic
    msqe_df = []
    msqe_df.append(ar1_res)
    msqe_df.append(rl_res)
    msqe_df.append(auto_res)
    msqe_df.append(dd_res)
    msqe_df.append(ic_res)
    msqe_df.append(avg_res)
    
    #Set Dataframes
    #Variance
    df_var[str(y)] = var_df
    df_var.index = ['ar1','rl','auto','dd','ic','avg'] 
    df_var.to_csv(r'C:\Dados\GIT pessoal\reinforcement-learning-forecast\Release\3.0.0\Monte Carlo\RL results\Variance and MSQE\variance_{}.csv'.format(j))
    
    #Msqe
    df_res[str(y)] = msqe_df
    df_res.index = ['ar1','rl','auto','dd','ic','avg'] 
    df_res.to_csv(r'C:\Dados\GIT pessoal\reinforcement-learning-forecast\Release\3.0.0\Monte Carlo\RL results\Variance and MSQE\msqe_{}.csv'.format(j))
    
    #Calculate Mean over data
df_res = pd.read_csv(r'C:\Dados\GIT pessoal\reinforcement-learning-forecast\Release\3.0.0\Monte Carlo\RL results\Variance and MSQE\msqe_499.csv',index_col=0)
df_var = pd.read_csv(r'C:\Dados\GIT pessoal\reinforcement-learning-forecast\Release\3.0.0\Monte Carlo\RL results\Variance and MSQE\variance_499.csv',index_col=0)
df_res.mean(axis=1)
df_var.mean(axis=1)
    
#----------------------------------------------------------------------------#
#Generate Statistics
import random
import pandas as pd
import numpy as np
import time
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import os


desc_stat = pd.read_csv('histogram_500_estab.csv')
desc_stat_result = desc_stat.loc[:, desc_stat.columns.str.contains('result_')]
desc_stat_df = pd.DataFrame()

for row in range(1,350):
    desc_stat_serie = desc_stat_result.iloc[row,:].value_counts()
    desc_stat_df.loc[:,'{}'.format(row)] = desc_stat_serie
    
desc_stat_df[:1].index.max()  


desc_stat_df.iloc[0,:].hist()


