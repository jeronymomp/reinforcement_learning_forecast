"""
File to Generate Final version to Reinforcement Learning

@author: Jeronymo
"""

#1 - Base File

#----------------------------------------------------------------------------#
#1.1 - Packages

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#----------------------------------------------------------------------------#
#1.2 - Get Files

file = pd.read_csv("Serie_1.csv")

#Exclude NAN
File = file.iloc[21:400,:]
File = File.reset_index()
File = File.iloc[:,2:len(File.columns)]
del file

#Set Squared Error

File.loc[:,'Ar1Error'] = File.loc[:,'Ar1Error']*File.loc[:,'Ar1Error']
File.loc[:,'DdError'] = File.loc[:,'DdError']*File.loc[:,'DdError']
File.loc[:,'IcError'] = File.loc[:,'IcError']*File.loc[:,'IcError']
File.loc[:,'AutoError'] = File.loc[:,'AutoError']*File.loc[:,'AutoError']
File.loc[:,'AvgForec_error'] = File.loc[:,'AvgForec_error']*File.loc[:,'AvgForec_error']


#----------------------------------------------------------------------------#
#1.3 - Operations

#Ar1
for j in range(len(File)-4):
     Serie = File.iloc[(0+j):(5+j),1].cumsum()
     Serie = Serie.reset_index(drop=True)
     Obs = Serie[len(Serie)-1]
     File.loc[(4+j),'ar1']  = Obs
     
#Double Difference
for j in range(len(File)-4):
     Serie = File.iloc[(0+j):(5+j),2].cumsum()
     Serie = Serie.reset_index(drop=True)
     Obs = Serie[len(Serie)-1]
     File.loc[(4+j),'dd']  = Obs
     
#IC
for j in range(len(File)-4):
     Serie = File.iloc[(0+j):(5+j),3].cumsum()
     Serie = Serie.reset_index(drop=True)
     Obs = Serie[len(Serie)-1]
     File.loc[(4+j),'ic']  = Obs
     
#Autometrics
for j in range(len(File)-4):
     Serie = File.iloc[(0+j):(5+j),4].cumsum()
     Serie = Serie.reset_index(drop=True)
     Obs = Serie[len(Serie)-1]
     File.loc[(4+j),'auto']  = Obs
     
#AvgForec
for j in range(len(File)-4):
     Serie = File.iloc[(0+j):(5+j),5].cumsum()
     Serie = Serie.reset_index(drop=True)
     Obs = Serie[len(Serie)-1]
     File.loc[(4+j),'avg_forec']  = Obs
     
#Extract only MAESQ columns
File = File.iloc[:,np.r_[0,6:11]]

#Get the mean
File.loc[:,'auto'] = File.loc[:,'auto'] / 5
File.loc[:,'ar1'] = File.loc[:,'ar1'] / 5
File.loc[:,'dd'] = File.loc[:,'dd'] / 5
File.loc[:,'ic'] = File.loc[:,'ic'] / 5
File.loc[:,'avg_forec'] = File.loc[:,'avg_forec'] / 5

del Obs
del Serie
del j
     
#----------------------------------------------------------------------------#
#1.4 Get minimun value column

for h in range(1,376):
    File.loc[(3+h),"total_result"] = File.iloc[(3+h),1:5].astype('float64').idxmin()

#result with AvgMean

for h in range(1,376):
    File.loc[(3+h),"result"] = File.iloc[(3+h),1:6].astype('float64').idxmin()


#----------------------------------------------------------------------------#
#1.5 Get Signal value column
for h in range(0,372):
    File.loc[(7+h),"Signal"] = str(File.iloc[(4+h),6]) + ',' + str(File.iloc[(5+h),6]) + ',' + str(File.iloc[(6+h),6])
    
#1.6 Adjusting data

Final = File.iloc[7:376,0:6]
Final.loc[:,"Signal"] = File.loc[:,"Signal"]
Final.loc[:,"result"] = File.loc[:,"result"]


Final.to_csv('agent_data_1.csv')


#----------------------------------------------------------------------------#
#2.0 For loop to generate Data

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os

#Get WD
#cwd = os.getcwd()

Data = range(1,501)

for d in Data:   
    file = pd.read_csv(r'C:\Dados\GIT pessoal\reinforcement-learning-forecast\Release\3.0.0\Monte Carlo\Ox Files\Results\Serie_{}.csv'.format(d))
    File = file.iloc[21:400,:]
    File = File.reset_index()
    File = File.iloc[:,2:len(File.columns)]
    del file
    File.loc[:,'Ar1Error'] = File.loc[:,'Ar1Error']*File.loc[:,'Ar1Error']
    File.loc[:,'DdError'] = File.loc[:,'DdError']*File.loc[:,'DdError']
    File.loc[:,'IcError'] = File.loc[:,'IcError']*File.loc[:,'IcError']
    File.loc[:,'AutoError'] = File.loc[:,'AutoError']*File.loc[:,'AutoError']
    File.loc[:,'AvgForec_error'] = File.loc[:,'AvgForec_error']*File.loc[:,'AvgForec_error']
    for j in range(len(File)-4):
         Serie = File.iloc[(0+j):(5+j),1].cumsum()
         Serie = Serie.reset_index(drop=True)
         Obs = Serie[len(Serie)-1]
         File.loc[(4+j),'ar1']  = Obs    
    for j in range(len(File)-4):
         Serie = File.iloc[(0+j):(5+j),2].cumsum()
         Serie = Serie.reset_index(drop=True)
         Obs = Serie[len(Serie)-1]
         File.loc[(4+j),'dd']  = Obs
    for j in range(len(File)-4):
         Serie = File.iloc[(0+j):(5+j),3].cumsum()
         Serie = Serie.reset_index(drop=True)
         Obs = Serie[len(Serie)-1]
         File.loc[(4+j),'ic']  = Obs
    for j in range(len(File)-4):
         Serie = File.iloc[(0+j):(5+j),4].cumsum()
         Serie = Serie.reset_index(drop=True)
         Obs = Serie[len(Serie)-1]
         File.loc[(4+j),'auto']  = Obs
    for j in range(len(File)-4):
        Serie = File.iloc[(0+j):(5+j),5].cumsum()
        Serie = Serie.reset_index(drop=True)
        Obs = Serie[len(Serie)-1]
        File.loc[(4+j),'avg_forec']  = Obs
    File = File.iloc[:,np.r_[0,6:11]]
    File.loc[:,'auto'] = File.loc[:,'auto'] / 5
    File.loc[:,'ar1'] = File.loc[:,'ar1'] / 5
    File.loc[:,'dd'] = File.loc[:,'dd'] / 5
    File.loc[:,'ic'] = File.loc[:,'ic'] / 5
    File.loc[:,'avg_forec'] = File.loc[:,'avg_forec'] / 5
    del Obs
    del Serie
    del j
    for h in range(1,376):
        File.loc[(3+h),"result"] = File.iloc[(3+h),1:6].astype('float64').idxmin()
    for h in range(0,372):
        File.loc[(7+h),"Signal"] = str(File.iloc[(4+h),6]) + ',' + str(File.iloc[(5+h),6]) + ',' + str(File.iloc[(6+h),6])
    Final = File.iloc[7:376,0:6]
    Final.loc[:,"Signal"] = File.loc[:,"Signal"]
    Final.loc[:,"result"] = File.loc[:,"result"]
    Final.to_csv(r'C:\Dados\GIT pessoal\reinforcement-learning-forecast\Release\3.0.0\Monte Carlo\Agent Data\agent_data_{}.csv'.format(d))
    del File
    del Final
    








