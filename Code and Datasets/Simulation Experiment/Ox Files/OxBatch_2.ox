#include <oxstd.oxh>
#import <packages/CATS/CATS>

main()
{
	//--- Ox code for CATS( 5)
	decl model = new CATS();

	model.Load("D:\\Dropbox\\FGV\\CEMAP\\pesquisas\\Jeronymo-ML\\previsoes.in7");
	model.Select("Y", {"Serie-1", 0, 0});
	model.Select("Y", {"Serie-3", 0, 0});
	model.Select("X", {"Serie-2", 0, 0});
	model.Select("X", {"Serie-4", 0, 0});
	model.Select("U", {"S1:1930:1", 0, 0});
//	model.Select("U", {"Constant", 0, 0});
//	model.Select("X", {"Trend", 0, 0});

	model.Lags(2,2,2);
	model.I1Rank(2);
	model.Trend("CIDRIFT");
	model.SetSelSample(1902, 1, 1949, 1);
	model.SetMethod("RRR");
	model.Estimate();

	model.SimulateRankTest(400, 1000);

	delete model;
}
