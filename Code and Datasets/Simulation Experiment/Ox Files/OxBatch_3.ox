#include <oxstd.oxh>
#import <packages/PcGive/pcgive>

main()
{
// This program requires a licenced version of PcGive Professional.
	//--- Ox code for EQ( 2)
	decl model = new PcGive();

	model.Load("D:\\Dropbox\\FGV\\CEMAP\\pesquisas\\Jeronymo-ML\\previsoes.in7");
	model.Deterministic(-1);

	// use CreateInterventions() first if the dummy variables do not exist yet:
//	model.CreateInterventions({"I:1914","S1:1920"});

	model.Select("Y", {"DSerie-1", 0, 0});
	model.Select("U", {"Constant", 0, 0});
	model.Select("X", {"I:1914", 0, 0});
	model.Select("X", {"S1:1920", 0, 0});
	// Formulation of the GUM (commented out)
/*
	model.DeSelect();
	model.Select("Y", {"DSerie-1", 0, 1});
	model.Select("U", {"Constant", 0, 0});
	model.Autometrics(0.01, "IIS+SIS+DIIS+TIS", 0);
*/
	model.SetSelSample(1902, 1, 1949, 1);
	model.SetMethod("OLS");
	model.Estimate();
	model.TestSummary();

	delete model;
}
